# Strings
name         = "alex"
last_name    = "cen"
job          = "developer"
special_char = "ñó" # encoding: utf-8 - optional?

# downcase / upcase / capitalize
# \n \t
# "".methods
puts "#{name.capitalize} #{last_name.capitalize} es #{job}"
puts special_char

# Output & Input
print "Ingresa tu nombre: "
param = gets
name = param.chomp
puts "Hola #{name}, tu nombre tiene #{name.length} letras."
