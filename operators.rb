# Operadores
print "Ingresa un número: "
param  = gets.chomp
number = param.to_i

if number.even?
    puts "#{number} es par."
else
    puts "#{number} es impar."
end

# puts "Comportamiento inverso al if" unless false
print "Ingresa tu edad: "
years_old = gets.chomp.to_i
puts "Eres menor de edad" unless years_old >= 18

# number_int   = 1
# number_float = 1.0

# puts number_int.eql?(number_float) / Comparación de tipos

# Operador ternario
print "Ingresa tu nombre de usuario: "
user_name = gets.chomp

puts (if user_name == "Admin" then "Administrador" else "Usuario" end)

response = if user_name == "Admin" then
    "Administrador"
else
    "Usuario"
end

puts response

puts user_name == "Admin" ? "Administrador" : "Usuario"
