range_numbers = (1..10)

range_numbers.each do |number|
    puts number
end

range_numbers.step(2).each do |num|
    puts num
end

('a'..'z').each do |char|
    print "#{char} "
end

# Mixin en arreglos y rangos => .min / .max
# puts range_numbers.min
# puts range_numbers.max

# Rango a arreglo => .to_a
# puts range_numbers.to_a.reverse