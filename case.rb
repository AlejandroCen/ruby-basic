# Case
print "Ingrese su calificación: "
score = gets.chomp.to_i

# range => when 8..10 / 9,10
case score
when 10
  puts "A"
when 9
  puts "B"
when 8
  puts "C"
when 7
  puts "D"
when 6
  puts "E"
else
  puts "F"
end