scores = [10, 5, 8, 9, 8, 9]

# puts scores

sum = 0

# .each / .each_with_index
scores.each_with_index do |score, index|
    # puts index
    sum += score
end

final_score = sum / scores.length

puts "Promedio: #{ final_score.to_f }"

# Obtener tipo de dato => .class.name
puts scores * 2             # Duplica el array
puts scores.join('-')       # Convierte un array a string separado por "-"
# puts scores * '-'         # Similar al "join"
# print scores.sort.reverse # Ordenar arreglo

puts scores.include?(10)    # Busca un valor dentro del arreglo
puts scores.first
puts scores.last
# print scores.uniq         # Regresa un arreglo sin valores repetidos
puts scores.sample          # Regresa un valor random 
