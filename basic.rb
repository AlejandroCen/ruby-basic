# Variables
name      = "Alex"
years_old = 22

puts name
puts years_old
puts name + " tiene " + String(years_old)

# Numbers
result_int   = 10 / 3
result_float = 10.0 / 3.0

# to_f / to_i
int_to_float = result_int.to_f
puts int_to_float

# -10.abs => 10
# 2.even? => true / Par o impar
